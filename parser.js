const parseArgv = input => {
  return input.reduce((acc, curr) => {
    const beforeSing = curr.substring(curr.indexOf('-') + 1, curr.indexOf('='));
    const afterSign = curr.substring(curr.indexOf('=') + 1);

    if (beforeSing !== '') {
      acc[beforeSing] = afterSign;
    }

    return acc;
  }, {});
};

module.exports = {
  parseArgv,
};

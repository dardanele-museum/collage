const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const mkdirp = require('mkdirp');
const path = require('path');
const isDev = require('electron-is-dev');
const fs = require('fs');
const ipcMain = electron.ipcMain;
const { uploadUserImage, userExistsInDB, login } = require('./api');
const { parseArgv } = require('./parser');

let mainWindow;

const getConfig = () => {
  return JSON.parse(
    fs.readFileSync(`${process.resourcesPath}/hilight.json`, 'utf8'),
  );
};

const config = getConfig();

const argv = parseArgv(process.argv.slice(1));
global.resourcesPath = process.resourcesPath;
global.argv = argv;

const createWindow = () => {
  const url = isDev
    ? 'http://localhost:3000'
    : `file://${path.join(__dirname, 'build/index.html')}`;

  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
  });
  mainWindow.loadURL(url);
  mainWindow.on('closed', () => (mainWindow = null));

  applySettings();
};

const applySettings = () => {
  mainWindow.removeMenu();

  if (!isDev) {
    mainWindow.fullScreen = config.fullScreen;
  }
};

app.whenReady().then(createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

const generateImagePath = userKey => {
  const fileName = new Date().getTime().toString()
  const dir = `${config.save.folder}${userKey}/`
  mkdirp.sync(dir);
  return `${userKey}/${fileName}.jpeg`;
};

const writeImageToFolder = (image, imagePath) => {
  try {
    const imageWithoutTags = image.replace(/^data:image\/jpeg;base64,/, '');
    const buffer = Buffer.from(imageWithoutTags, 'base64');
    fs.writeFileSync(imagePath, buffer);
  } catch (e) {
    console.log('Error while saving image: ', e);
  }
};

ipcMain.handle('saveImage', async (event, ...args) => {
  const { founded, userKey } = await userExistsInDB({
    host: config.api.host,
    email: args[0].email,
  });
  if( founded === true ){
    const imagePath = generateImagePath( userKey );
    const result = await uploadUserImage({
      host: config.api.host,
      appName: config.app.name,
      userKey,
      mediaPaths: [`${config.save.db}${imagePath}`],
    });

    if (result === 200) {
      writeImageToFolder(args[0].imageBase64, `${config.save.folder}${imagePath}`);
    }
  }

  mainWindow.webContents.send('saveImageResult', {
    founded,
    userKey
  })

});

login(config.api);

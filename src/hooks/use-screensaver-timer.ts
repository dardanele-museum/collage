import { useEffect, useRef } from 'react';
import { GAME_STATES } from '../statemachine/state-machine';
import { screensaver } from '../config.json'

const SCREENSAVER_APPEAR_TIME = screensaver.time * 1000;

export const useScreensaverTimer = ({
  state,
  goToScreensaver,
}: {
  state?: GAME_STATES;
  goToScreensaver: () => void;
}) => {
  const timeoutObj = useRef<any>(null);

  useEffect(() => {
    if (state === GAME_STATES.SUMMARY || state === GAME_STATES.INIT) {
      timeoutObj.current = setTimeout(goToScreensaver, SCREENSAVER_APPEAR_TIME);
    } else {
      clearTimeout(timeoutObj.current);
    }
  }, [goToScreensaver,state]);
};

import { useCallback } from 'react';
const ASSETS_FOLDER = 'assets';
const PRESENTATION_JSON = 'presentation.json';

export const useApiCall = ({
  resourcesPath = '',
}: {
  resourcesPath?: string;
}) => {
  const getTranslations = useCallback(async () => {
    const result = await fetch(
      resourcesPath
        ? `${resourcesPath}/${PRESENTATION_JSON}`
        : `${ASSETS_FOLDER}/${PRESENTATION_JSON}`,
    );
    return await result.json();
  }, [resourcesPath]);

  return {
    getTranslations,
  };
};

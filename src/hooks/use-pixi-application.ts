import { useCallback, useEffect, useState } from 'react';
import { Application, Rectangle, SCALE_MODES } from 'pixi.js';

let ipcRenderer: any = null;
if (window.require) {
  ipcRenderer = window.require('electron').ipcRenderer;
}

export const usePixiApplication = ({
  width,
  height,
  parent,
}: {
  width: number;
  height: number;
  parent: any;
}) => {
  const [application] = useState<Application>(
    new Application({
      width,
      height,
      backgroundColor: 0x000000,
      transparent: false,
    }),
  );

  useEffect(() => {
    const DOMNode = parent.current;
    return () => {
      if (application && DOMNode) {
        DOMNode.removeChild(application.view);
        application?.destroy(true);
      }
    };
  }, [application, parent]);

  useEffect(() => {
    if (parent && parent.current) {
      parent.current.appendChild(application.view);
    }
  }, [parent, application]);

  const prepareScreenshotAndSend = useCallback(
    (email: string) => {
      const texture = application.renderer.generateTexture(
        application.stage,
        SCALE_MODES.LINEAR,
        0,
        new Rectangle(0, 0, width, height),
      );
      const image = application.renderer.plugins.extract.base64(
        texture,
        'image/jpeg',
      );

      if (ipcRenderer) {
        ipcRenderer.invoke('saveImage', {
          imageBase64: image,
          email,
        });
      }
    },
    [application, width, height],
  );

  return { application, prepareScreenshotAndSend };
};

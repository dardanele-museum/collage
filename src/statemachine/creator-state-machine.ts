import { Machine } from 'xstate';

export enum CREATOR_STATES {
  INIT = 'init',
  INFO = 'info',
  EMAIL = 'email',
}

export enum CREATOR_TRANSITIONS {
  SHOW_INFO_POPUP = 'showInfo',
  SHOW_EMAIL_POPUP = 'showEmailPopup',
  HIDE_POPUP = 'hidePopup'
}

export const CreatorMachine = Machine({
  id: 'gameStates',
  initial: CREATOR_STATES.INIT,
  states: {
    [CREATOR_STATES.INIT]: {
      on: {
        [CREATOR_TRANSITIONS.SHOW_EMAIL_POPUP]: CREATOR_STATES.EMAIL,
        [CREATOR_TRANSITIONS.SHOW_INFO_POPUP]: CREATOR_STATES.INFO
      },
    },
    [CREATOR_STATES.INFO]: {
      on: {
        [CREATOR_TRANSITIONS.HIDE_POPUP]: CREATOR_STATES.INIT,
      }
    },
    [CREATOR_STATES.EMAIL]: {
      on: {
        [CREATOR_TRANSITIONS.HIDE_POPUP]: CREATOR_STATES.INIT
      }
    }
  },
});

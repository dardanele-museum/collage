import { Machine } from 'xstate';

export enum GAME_STATES {
  INIT = 'init',
  CREATOR = 'creator',
  SUMMARY = 'summary',
  SCREENSAVER = 'screensaver'
}

export enum TRANSITIONS {
  START_CREATOR = 'startCreator',
  SHOW_INIT_SCREEN = 'showInitScreen',
  SHOW_SCREENSAVER = 'showScreensaver',
  GO_BACK = 'goBack'
}

export const GameMachine = Machine({
  id: 'gameStates',
  initial: GAME_STATES.SCREENSAVER,
  states: {
    [GAME_STATES.INIT]: {
      on: {
        [TRANSITIONS.START_CREATOR]: GAME_STATES.CREATOR,
        [TRANSITIONS.SHOW_SCREENSAVER]: GAME_STATES.SCREENSAVER
      },
    },
    [GAME_STATES.CREATOR]: {
      on: {
        [TRANSITIONS.GO_BACK]: GAME_STATES.INIT,
      }
    },
    [GAME_STATES.SCREENSAVER]: {
      on: {
        [TRANSITIONS.SHOW_INIT_SCREEN]: GAME_STATES.INIT
      }
    }
  },
});

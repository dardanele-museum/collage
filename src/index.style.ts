import styled from 'styled-components';

export const ApplicationWrapper = styled.div<{ language: string }>`
  white-space: pre-line;
  vertical-align: bottom;
  user-select: none;
  font-family: ${props =>
    props.language === 'ru' ? 'NocturneSemiBold' : 'ManofaMedium'};
`;

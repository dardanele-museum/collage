import React, {useCallback} from 'react';
import { useMachine } from '@xstate/react';
import { useScreensaverTimer } from '../hooks/use-screensaver-timer';
import {
  GAME_STATES,
  GameMachine,
  TRANSITIONS,
} from '../statemachine/state-machine';
import { ArtistsScreen } from './screens/artistscreen/artist-screen';
import { Screen } from './screens/screen';
import { CreatorScreenPixi } from './screens/creatorscreen/creator-screen-pixi';
import { ScreensaverScreen } from './screens/screensaver/screensaver-screen';
import { app as appConfig } from '../config.json';

export function Application() {
  const [state, send] = useMachine(GameMachine);
  const { width: appWidth, height: appHeight } = appConfig;

  useScreensaverTimer({
    state: state.value as any,
    goToScreensaver: () => send(TRANSITIONS.SHOW_SCREENSAVER),
  });

  const applyTransition = useCallback(
      (destination: TRANSITIONS) => () => {
        send(destination);
      },
      [send],
  );

  return (
    <Screen showBackground={true}>
      <>
        {state.value === GAME_STATES.INIT && (
          <ArtistsScreen
            onStart={applyTransition(TRANSITIONS.START_CREATOR)}
          />
        )}
        {state.value === GAME_STATES.CREATOR && (
          <CreatorScreenPixi
            width={appWidth}
            height={appHeight}
            onBackClick={applyTransition(TRANSITIONS.GO_BACK)}
          />
        )}
        {state.value === GAME_STATES.SCREENSAVER && (
          <ScreensaverScreen
            onClick={applyTransition(TRANSITIONS.SHOW_INIT_SCREEN)}
          />
        )}
      </>
    </Screen>
  );
}

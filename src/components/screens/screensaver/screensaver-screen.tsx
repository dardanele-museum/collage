import React from 'react';
import { Screen } from '../screen';
import { Image, Text } from './screensaver.style';
import { useTranslation } from '../../../hooks/use-translation';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { Wrapper } from '../screen.style';

export function ScreensaverScreen({ onClick }: { onClick: () => void }) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-screensaver',
      base: 'screensavers'
    },
  ]);

  if (!moduleTranslations) return null;

  const t = moduleTranslations['collage-screen-screensaver'];

  return (
    <Screen showBackground={false} onClick={onClick}>
      <Wrapper>
        <Image />
        <Text top={90} left={500} delay={1}>
          {t['theatre'][language]}
        </Text>
        <Text top={200} left={350} delay={3}>
          {t['assemblage'][language]}
        </Text>
        <Text top={600} left={100} size={70} delay={5}>
          {t['kantor'][language]}
        </Text>
        <Text top={850} left={110} delay={8}>
          {t['symbol'][language]}
        </Text>
        <Text top={990} left={230} delay={10}>
          {t['object'][language]}
        </Text>
        <Text top={1230} left={230} delay={2}>
          {t['trauma'][language]}
        </Text>
        <Text top={1100} left={730} delay={4}>
          {t['process'][language]}
        </Text>
        <Text top={1450} left={180} delay={8}>
          {t['actor'][language]}
        </Text>
        <Text top={1480} left={580} size={70} delay={6}>
          {t['szajna'][language]}
        </Text>
        <Text top={1650} left={480} delay={7}>
          {t['collage'][language]}
        </Text>
      </Wrapper>
    </Screen>
  );
}

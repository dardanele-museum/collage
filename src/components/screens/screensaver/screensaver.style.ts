import styled, { keyframes } from 'styled-components';
import screensaver from '../../../assets/images/screensaver.png';

export const Image = styled.div`
  position: absolute;
  background: url(${screensaver}) no-repeat center;
  width: 1080px;
  height: 1920px;
`;

const blur = keyframes`
  0% {
    filter: blur(0px);
  }

  50% {
    filter: blur(6px);
  }
  100% {
    filter: blur(0px);
  }
`;

export const Text = styled.div<{ top: number, left: number, size?: number, delay?: number }>`
  position: absolute;
  top: ${ props => props.top}px;
  left: ${ props => props.left}px;
  color: #000000;
  font-size: ${props => props.size ? props.size : 60}px;
  font-family: HelveticaStandard;
  animation: ${blur} 6s linear infinite;
  animation-delay: ${props => props.delay ? props.delay : 0}s;
`




import React, { useCallback, useState } from 'react';
import { Screen } from '../screen';
import { ControlButtons } from '../../ui/controlbuttons/controlbuttons';
import { BackButton, TopWrapper, Wrapper } from './creator-screen.style';
import { Thumbnail } from '../../ui/thumbnail/thumbnail';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { EmailScreen } from '../emailscreen/email-screen';
import { useMachine } from '@xstate/react';
import {
  CREATOR_STATES,
  CREATOR_TRANSITIONS,
  CreatorMachine,
} from '../../../statemachine/creator-state-machine';
import { GAME_STATES } from '../../../statemachine/state-machine';
import { ArtistInfoScreen } from '../artistinfoscreen/artistinfo-screen';
import { Application } from 'pixi.js';
import { useInitPixiObjects } from '../../pixi/hooks/use-init-pixi-objects';
import { useModifyComposition } from '../../pixi/hooks/use-modify-composition';
import { backgrounds, foils } from '../../../assets/data/data.json';

let ipcRenderer: any = null;
if (window.require) {
  ipcRenderer = window.require('electron').ipcRenderer;
}

export function CreatorScreenUI({
  state,
  onBackClick,
  onSend,
  application,
  children,
}: {
  state?: GAME_STATES;
  onBackClick: () => void;
  onSend: (email: string) => void;
  application: Application | null;
  children: React.ReactChild;
}) {
  const {
    game: { artist },
  } = useAppContext();

  const [creatorState, send] = useMachine(CreatorMachine);
  const { creator } = useInitPixiObjects({ application, artist });
  const { reset, toggleFoil, changeBackground } = useModifyComposition({
    creator,
    foils,
    backgrounds,
  });
  const [saveStatus, setSaveStatus] = useState<null | boolean>(null);

  const applyTransition = useCallback(
    (destination: CREATOR_TRANSITIONS) => () => {
      send(destination);
    },
    [send],
  );

  const hideEmailPopupAndSend = useCallback(
    async (email: string) => {
      onSend(email);
      if (ipcRenderer) {
        ipcRenderer.on('saveImageResult', (event: any, message: any) => {
          setSaveStatus(message.founded);
          if (message.founded === true) {
            setTimeout(() => {
              send(CREATOR_TRANSITIONS.HIDE_POPUP);
            }, 2000);
          }else{
            setTimeout(() => {
              setSaveStatus(null);
            }, 2000);
          }
        });
      }
    },
    [send, onSend],
  );

  return (
    <Screen state={state} showBackground={false}>
      <>
        <Wrapper>
          {children}
          <TopWrapper>
            <BackButton onClick={onBackClick} />
            <Thumbnail
              onClick={applyTransition(CREATOR_TRANSITIONS.SHOW_INFO_POPUP)}
            />
          </TopWrapper>
          {creatorState.value === CREATOR_STATES.INIT && (
            <ControlButtons
              onBackgroundClick={changeBackground}
              onFoilClick={toggleFoil}
              onSendClick={applyTransition(
                CREATOR_TRANSITIONS.SHOW_EMAIL_POPUP,
              )}
              onResetClick={reset}
              creator={creator}
            />
          )}
        </Wrapper>
        {creatorState.value === CREATOR_STATES.EMAIL && (
          <EmailScreen
            onClose={applyTransition(CREATOR_TRANSITIONS.HIDE_POPUP)}
            onSend={hideEmailPopupAndSend}
            saveStatus={saveStatus}
          />
        )}
        {creatorState.value === CREATOR_STATES.INFO && (
          <ArtistInfoScreen
            onClose={applyTransition(CREATOR_TRANSITIONS.HIDE_POPUP)}
          />
        )}
      </>
    </Screen>
  );
}

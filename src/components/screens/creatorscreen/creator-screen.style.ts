import styled from 'styled-components';
import back from '../../../assets/images/back.jpg';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 980px;
  height: 1820px;
  padding: 50px;
`;

export const TopWrapper = styled.div`
  display: flex;
  position: relative;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  pointer-events: none;
`;

export const BackButton = styled.div`
  background: url(${back}) no-repeat center;
  width: 50px;
  height: 50px;
  cursor: pointer;
  pointer-events: all;
`;

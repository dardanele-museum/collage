import React, { useRef } from 'react';
import { usePixiApplication } from '../../../hooks/use-pixi-application';
import { CreatorScreenUI } from './creator-screen-ui';

export function CreatorScreenPixi({
  width,
  height,
  onBackClick,
}: {
  width: number;
  height: number;
  onBackClick: () => void;
}) {
  const ref = useRef(null);

  const { application, prepareScreenshotAndSend } = usePixiApplication({
    width,
    height,
    parent: ref,
  });

  return (
    <CreatorScreenUI
      onBackClick={onBackClick}
      application={application}
      onSend={prepareScreenshotAndSend}>
      <div
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width,
          height,
        }}
        ref={ref}
      />
    </CreatorScreenUI>
  );
}

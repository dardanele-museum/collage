import { GAME_STATES } from '../../../statemachine/state-machine';
import { Screen } from '../screen';
import React from 'react';
import { Wrapper } from '../screen.style';
import { Artist } from './artist-screen.style';
import { ARTISTS, useAppContext } from '../../appdata/hooks/use-app-context';
import { Button } from '../../ui/button/button';
import { useTranslation } from '../../../hooks/use-translation';

export function ArtistsScreen({
  onStart,
  state,
}: {
  onStart: () => void;
  state?: GAME_STATES;
}) {
  const {
    game: { chooseArtist },
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-artists',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-artists'],
    },
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['collage-screen-artists'];

  return (
    <Screen state={state} showBackground={false}>
      <Wrapper>
        <Artist
          onClick={() => {
            chooseArtist(ARTISTS.KANTOR);
            onStart();
          }}
          url={'assets/images/artists/kantor_start.png'}
          bgPosition={'bottom'}>
          <Button
            title={t['kantor'][language]}
            subtitle={t['assemblage'][language]}
            top={600}
            language={language}
          />
        </Artist>
        <Artist
          onClick={() => {
            chooseArtist(ARTISTS.SZAJNA);
            onStart();
          }}
          url={'assets/images/artists/szajna_start.png'}
          bgPosition={'top'}>
          <Button
            top={450}
            title={t['szajna'][language]}
            subtitle={t['collage'][language]}
            language={language}
          />
        </Artist>
      </Wrapper>
    </Screen>
  );
}

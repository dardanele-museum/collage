import styled from 'styled-components';

export const Artist = styled.div<{ url: string; bgPosition: 'top' | 'bottom' }>`
  position: relative;
  background: url(${props => props.url}) no-repeat ${props => props.bgPosition};
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  cursor: pointer;
`;

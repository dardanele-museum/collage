import styled from 'styled-components';
import close from "../../assets/images/close.jpg";

export const Container = styled.div`
  white-space: pre-line;
  vertical-align: bottom;
  user-select: none;
`;

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 1080px;
  height: 1920px;
`;

export const AbsoluteWrapper = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  width: 1080px;
  height: 1920px;
  flex-direction: column;
  align-items: center;
`;

export const Background = styled.div`
  position: absolute;
  background-color: #000000;
`;

export const CloseButton = styled.div`
  width: 60px;
  height: 60px;
  position: absolute;
  top: 0;
  right: 0;
  background: url(${close}) no-repeat center;
  cursor: pointer;
`
export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  background-color: #000000;
  width: 1080px;
  height: 1920px;
  opacity: 60%;
`;

type SpacerType = {
    width?: number;
    height?: number;
};

export const Spacer = styled.div<SpacerType>`
  min-height: ${props => (props.height === undefined ? 0 : props.height)}px;
  width: ${props => (props.width === undefined ? 0 : props.width)}px;
`;

import React from 'react';
import { Screen } from '../screen';
import {
  AboutLeftColumn,
  AboutRightColumn,
  AboutRightColumnDescription,
  AboutRightColumnImage,
  AboutRightColumnTitle,
  AboutWrapper,
  InfoWrapper,
  Title,
  TopBar,
  TopBarBlock,
} from './artistinfo-screen.style';
import { AbsoluteWrapper, CloseButton, Overlay, Spacer } from '../screen.style';
import { ARTISTS, useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';
import { useScrollTo } from './hooks/use-scroll-to';
// @ts-ignore
import { Element } from 'react-scroll';

export function ArtistInfoScreen({ onClose }: { onClose: () => void }) {
  const {
    app: { translations, language },
    game: { artist },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-artist-info-szajna',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-artist-info-szajna'],
    },
    {
      name: 'collage-screen-artist-info-kantor',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-artist-info-kantor'],
    },
  ]);

  const { scrollTo } = useScrollTo();

  if (!moduleTranslations) return null;

  const t =
    moduleTranslations[
      artist === ARTISTS.KANTOR
        ? 'collage-screen-artist-info-kantor'
        : 'collage-screen-artist-info-szajna'
    ];

  return (
    <Screen showBackground={false}>
      <>
        <AbsoluteWrapper>
          <Overlay onClick={onClose} />
          <Spacer height={350} />
          <InfoWrapper>
            <CloseButton onClick={onClose} />
            <Title>{t['title'][language]}</Title>
            <TopBar>
              <TopBarBlock
                backgroundColor={'#3db6f5'}
                onClick={scrollTo('part1')}>
                {t['top-bar-1'][language]}
              </TopBarBlock>
              <TopBarBlock
                backgroundColor={'#0058bc'}
                onClick={scrollTo('part2')}>
                {t['top-bar-2'][language]}
              </TopBarBlock>
              <TopBarBlock
                backgroundColor={'#3db6f5'}
                onClick={scrollTo('part3')}>
                {t['top-bar-3'][language]}
              </TopBarBlock>
            </TopBar>
            <Spacer height={55} />
            <AboutWrapper>
              <AboutLeftColumn>{t['left-column'][language]}</AboutLeftColumn>
              <Spacer width={40} />
              <AboutRightColumn id={'rightColumn'}>
                <Element name={'part1'}>
                  <AboutRightColumnTitle>
                    {t['right-column-title'][language]}
                  </AboutRightColumnTitle>
                </Element>
                <Spacer height={20} />
                <AboutRightColumnDescription>
                  {t['right-column-description-part-1'][language]}
                </AboutRightColumnDescription>

                <Spacer height={20} />
                <AboutRightColumnImage>
                  <img
                    src={
                      artist === ARTISTS.KANTOR
                        ? 'assets/images/artists/kantor_info.jpg'
                        : 'assets/images/artists/szajna_info.jpg'
                    }
                    alt={'artist'}
                  />
                </AboutRightColumnImage>
                <Spacer height={20} />
                <Element name={'part2'}>
                  <AboutRightColumnDescription>
                    {t['right-column-description-part-2'][language]}
                  </AboutRightColumnDescription>
                </Element>
                <Spacer height={20} />
                <Element name={'part3'}>
                  <AboutRightColumnDescription>
                    {t['right-column-description-part-3'][language]}
                  </AboutRightColumnDescription>
                </Element>
              </AboutRightColumn>
            </AboutWrapper>
          </InfoWrapper>
        </AbsoluteWrapper>
      </>
    </Screen>
  );
}

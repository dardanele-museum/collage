import { useCallback } from 'react';
// @ts-ignore
import { scroller } from 'react-scroll';

export const useScrollTo = () => {
  const scrollTo = useCallback(
    (id: string) => () => {
      scroller.scrollTo(id, {
        duration: 500,
        smooth: true,
        containerId: 'rightColumn',
      });
    },
    [],
  );

  return {
    scrollTo,
  };
};

import styled from 'styled-components';

export const InfoWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  width: 870px;
  padding-left: 50px;
  padding-right: 50px;
  height: 1520px;
  background-color: #d2d2d2;
`;

export const Title = styled.div`
  display: flex;
  align-items: center;
  font-size: 50px;
  color: #494949;
  min-height: 180px;
`;

export const TopBar = styled.div`
  display: flex;
  width: 100%;
  min-height: 55px;
`;

export const TopBarBlock = styled.div<{ backgroundColor: string }>`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  background-color: ${props => props.backgroundColor};
  color: #ffffff;
  cursor: pointer;
`;

export const AboutWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 1180px;
`;

export const AboutLeftColumn = styled.div`
  display: flex;
  font-size: 30px;
  flex: 1;
`;

export const AboutRightColumn = styled.div`
  display: flex;
  font-size: 30px;
  font-family: ManofaCondensed;
  flex-direction: column;
  flex: 3;
  height: 100%;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

export const AboutRightColumnTitle = styled.div`
  font-size: 30px;
  font-family: HelveticaBold;
`;

export const AboutRightColumnDescription = styled.div`
  font-size: 30px;
  font-family: HelveticaStandard;
`;

export const AboutRightColumnImage = styled.div`
  width: 100%;
  height: 340px;
`;

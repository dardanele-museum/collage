import React, { useCallback, useState } from 'react';
import { Screen } from '../screen';
import {
  EmailWrapper,
  EmailTitle,
  EmailInput,
  SendWrapper,
  SendResultMessageOK,
  SendResultMessageError,
} from './email.style';
import { Button } from '../../ui/button/button';
import { AbsoluteWrapper, Overlay, Spacer } from '../screen.style';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';
import { validate } from 'email-validator';
import Keyboard from 'react-simple-keyboard';
import 'react-simple-keyboard/build/css/index.css';
import './style/keyboard.css';

const layout = {
  default: [
    'q w e r t y u i o p',
    'a s d f g h j k l',
    '{shift} z x c v b m n {bksp}',
    '{123} @ . {space}',
  ],
  shift: [
    'Q W E R T Y U I O P',
    'A S D F G H J K L',
    '{shift} Z X C V B M N {bksp}',
    '{123} @ . {space}',
  ],
  numbers: [
    '1 2 3 4 5 6 7 8 9 0',
    "^ * ( ) - _ + & '",
    '{shift} . ! ? ; : [ ] {bksp}',
    '{ABC} @ . {space}',
  ],
};

export function EmailScreen({
  onClose,
  onSend,
  saveStatus,
}: {
  onClose: () => void;
  onSend: (email: string) => void;
  saveStatus: boolean | null;
}) {
  const {
    app: { language, translations },
  } = useAppContext();

  const [email, setEmail] = useState('');
  const [layoutName, setLayoutName] = useState('default');

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-creator',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-creator'],
    },
  ]);

  const onKeyPress = useCallback(
    (button: string) => {
      if (button === '{shift}') {
        layoutName === 'default'
          ? setLayoutName('shift')
          : setLayoutName('default');
      }
      if (button === '{123}') {
        setLayoutName('numbers');
      }
      if (button === '{ABC}') {
        setLayoutName('default');
      }
    },
    [layoutName],
  );

  const onKeyboardChange = useCallback((value: string) => {
    setEmail(value);
  }, []);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['collage-screen-creator'];

  return (
    <Screen showBackground={false}>
      <>
        <AbsoluteWrapper>
          <Overlay onClick={onClose} />
          <Spacer height={420} />
          <EmailWrapper>
            <Spacer height={50} />
            <EmailTitle>{t['your-email'][language]}</EmailTitle>
            <Spacer height={80} />
            <EmailInput
              valid={validate(email)}
              value={email}
              onChange={e => {
                setEmail(e.target.value);
              }}
            />
            <Spacer height={60} />
            <Keyboard
              display={{
                '{bksp}': '&#8592;',
                '{shift}': '&#8593;',
                '{123}': '123',
                '{ABC}': 'ABC',
                '{space}': ' ',
              }}
              theme={'hg-theme-default customTheme'}
              onChange={onKeyboardChange}
              onKeyPress={onKeyPress}
              layout={layout}
              layoutName={layoutName}
              buttonTheme={[
                {
                  class: 'hg-button-shift',
                  buttons: '{shift}',
                },
                {
                  class: 'hg-button-space',
                  buttons: '{space}',
                },
                {
                  class: 'hg-button-backspace-and-ctrl',
                  buttons: '{bksp} {123} {ABC}',
                },
                {
                  class: 'hg-button-left-margin-big',
                  buttons: 'a A ^',
                },
                {
                  class: 'hg-button-right-margin-big',
                  buttons: "l L '",
                },
                {
                  class: 'hg-button-left-margin-small',
                  buttons:
                    "w W e E r R t T y Y u U i I o O p P s S d D f F g G h H j J k K l L z Z x X c C v V b B m M n N {bksp} @ {space} 2 3 4 5 6 7 8 9 0 * ( ) - _ + . ! ? ; : [ ] ' &",
                },
                {
                  class: 'hg-button-top-margin-small',
                  buttons:
                    "a A s S d D f F g G h H j J k K l L z Z x X c C v V b B m M n N {shift} {123} {ABC} {bksp} @ {space} . , ! ? ; : [ ] ^ * ( ) - _ + - ' &",
                },
                {
                  class: 'hg-button-l',
                  buttons: 'l L',
                },
              ]}
            />
            <Spacer height={100} />
            <SendWrapper>
              <Button
                fontSize={language === 'ru' ? 30 : 40}
                title={t['send'][language]}
                onClick={
                  validate(email)
                    ? () => {
                        onSend(email);
                      }
                    : undefined
                }
                active={validate(email)}
                colorInactive={'#008eff'}
              />
            </SendWrapper>
            {saveStatus !== null &&
              (saveStatus === true ? (
                <SendResultMessageOK>
                  {t['email-sent'][language]}
                </SendResultMessageOK>
              ) : (
                <SendResultMessageError>
                  {t['email-error'][language]}
                </SendResultMessageError>
              ))}
          </EmailWrapper>
        </AbsoluteWrapper>
      </>
    </Screen>
  );
}

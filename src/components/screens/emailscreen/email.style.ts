import styled from 'styled-components';

export const EmailWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  width: 970px;
  padding-left: 50px;
  padding-right: 50px;
  height: 1050px;
  background-color: #011a29;
`;

export const EmailTitle = styled.div`
  display: flex;
  align-self: center;
  font-size: 80px;
  color: #008eff;
`;

export const EmailInput = styled.input<{ valid?: boolean }>`
  width: 100%;
  height: 80px;
  font-size: 40px;
  color: #ffffff;
  background-color: #011a29;
  border: 5px solid #008eff;
  font-family: 'Manofa Medium';
`;

export const SendWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const SendResultMessageOK = styled.div`
  display: flex;
  justify-content: flex-end;
  color: white;
  margin-top: 20px;
  font-size: 30px;
`;

export const SendResultMessageError = styled.div`
  display: flex;
  justify-content: flex-end;
  color: red;
  margin-top: 20px;
  font-size: 30px;
`;

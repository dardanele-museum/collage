import React from 'react';
import { Container, Background } from './screen.style';
import { useScreenBackground } from './hooks/use-screen-background';
import { GAME_STATES } from '../../statemachine/state-machine';

export function Screen({
  onClick,
  children,
  showBackground = true,
  state,
}: {
  onClick?: () => void;
  children: React.ReactElement;
  showBackground?: boolean;
  state?: GAME_STATES;
}) {
  const { background } = useScreenBackground(state);

  return (
    <Container onClick={onClick ? onClick : undefined}>
      {showBackground && (
        <Background>
          <img src={background} alt={'background'} />
        </Background>
      )}
      {children}
    </Container>
  );
}

import {
  BLEND_MODES,
  Container,
  InteractionEvent,
  Loader,
  Point,
  Sprite,
} from 'pixi.js';

export class Creator extends Container {
  loader: Loader;
  objects: string[];
  currentSprite: Sprite | null = null;
  offset: Point | null = null;
  objectsSprite: Sprite = new Sprite();
  bgSprite: Sprite = new Sprite();
  foils: string[] = [];

  constructor({
    loader,
    objects,
    initBackground,
  }: {
    loader: Loader;
    objects: string[];
    initBackground: string;
  }) {
    super();
    this.loader = loader;
    this.objects = objects;
    this.interactive = true;
    this.addChild(this.bgSprite);
    this.addChild(this.objectsSprite);
    this.addObjects();
    this.changeBackground(initBackground);
  }

  addObjects() {
    this.objects.forEach(object => {
      const sprite = new Sprite();
      sprite.texture = this.loader.resources[object].texture;
      sprite.anchor.set(0.5);
      sprite.x = 1080 / 2;
      sprite.y = 1920 / 2;
      sprite.interactive = true;
      sprite.addListener('pointerdown', this.onDown, this);
      this.objectsSprite.addChild(sprite);
    });
  }

  onDown(e: InteractionEvent) {
    this.offset = e.data.getLocalPosition(e.currentTarget);
    this.currentSprite = e.currentTarget as Sprite;
    this.objectsSprite.setChildIndex(
      this.currentSprite,
      this.objectsSprite.children.length - 1,
    );
    this.addListener('pointermove', this.onMove, this);
    this.addListener('pointerup', this.onUp, this);
  }

  onMove(e: InteractionEvent) {
    const { x, y } = e.data.global;
    if (this.currentSprite && this.offset) {
      this.currentSprite.position.set(x - this.offset.x, y - this.offset.y);
    }
  }

  onUp() {
    this.removeListener('pointermove', this.onMove, this);
    this.removeListener('pointerup', this.onUp, this);
  }

  changeBackground(name: string) {
    this.bgSprite.removeChildren();
    const bg = new Sprite();
    bg.texture = this.loader.resources[name].texture;
    this.bgSprite.addChild(bg);
  }

  toggleFoil(name: string) {
    const foilIndex = this.foils.indexOf(name);
    if (foilIndex >= 0) {
      this.objectsSprite.removeChild(this.objectsSprite.getChildByName(name));
      this.foils.splice(foilIndex, 1);
    } else {
      const foil = new Sprite();
      foil.name = name;
      foil.interactive = true;
      foil.blendMode = BLEND_MODES.MULTIPLY;
      foil.texture = this.loader.resources[name].texture;
      foil.addListener('pointerdown', this.onDown, this);
      this.objectsSprite.addChild(foil);
      this.foils.push(name);
    }
  }

  reset() {
    this.objectsSprite.removeChildren();
    this.addObjects();
    this.foils = [];
  }

  addCanvasClickListener(onCanvasClick: () => void) {
    this.addListener('pointerdown', onCanvasClick, this);
  }

  generateImage() {}
}

import { Loader } from 'pixi.js';
import { useEffect, useRef, useState } from 'react';
import { ARTISTS } from '../../appdata/hooks/use-app-context';
import data from '../../../assets/data/data.json';

export const usePreloadAssets = (artist: ARTISTS | null) => {
  const [preloaded, setPreloaded] = useState(false);
  const loader = useRef<Loader>(new Loader());

  useEffect(() => {
    if (artist) {
      const images = data.artists[artist];
      const backgrounds = data.backgrounds;
      const foils = data.foils;
      backgrounds.forEach(bg => {
        loader.current.add(bg, bg);
      });
      foils.forEach(foil => {
        loader.current.add(foil, foil);
      });
      images.forEach(image => {
        loader.current.add(image, image);
      });
      loader.current.onComplete.add(() => {
        setPreloaded(true);
      });
      loader.current.load();
    }
  }, [artist]);

  return {
    loader: loader.current,
    preloaded,
  };
};

import { Application } from 'pixi.js';
import { useCallback, useEffect, useState } from 'react';
import { Creator } from '../creator/creator';
import { usePreloadAssets } from './use-preload-assets';
import { ARTISTS } from '../../appdata/hooks/use-app-context';
import data from '../../../assets/data/data.json';

export const useInitPixiObjects = ({
  application,
  artist,
}: {
  application: Application | null;
  artist: ARTISTS | null;
}) => {
  const [creator, setCreator] = useState<Creator | null>(null);
  const { preloaded, loader } = usePreloadAssets(artist);

  useEffect(() => {
    if (application && preloaded && artist) {
      const c = new Creator({
        loader,
        objects: data.artists[artist],
        initBackground: data.backgrounds[0],
      });
      application.stage.addChild(c);
      setCreator(c);
    }
  }, [artist, loader, preloaded, application]);

  const saveImage = useCallback(() => {
    if (application && creator) {
      const image = application.renderer.extract.image(creator);
      document.body.appendChild(image);
    }
  }, [application, creator]);

  return {
    creator,
    saveImage,
  };
};

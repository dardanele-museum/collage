import { Creator } from '../creator/creator';
import { useCallback, useEffect, useRef } from 'react';

export const useModifyComposition = ({
  creator,
  foils,
  backgrounds,
}: {
  creator: Creator | null;
  foils: string[];
  backgrounds: string[];
}) => {
  const creatorInternal = useRef<null | Creator>(null);

  useEffect(() => {
    if (creator !== null) {
      creatorInternal.current = creator;
    }
  }, [creator]);

  const toggleFoil = useCallback(
    (foilIndex: number) => {
      if (creatorInternal && creatorInternal.current) {
        creatorInternal.current.toggleFoil(foils[foilIndex]);
      }
    },
    [foils],
  );

  const changeBackground = useCallback(
    (bgIndex: number) => {
      if (creatorInternal && creatorInternal.current) {
        creatorInternal.current.changeBackground(backgrounds[bgIndex]);
      }
    },
    [backgrounds],
  );

  const reset = useCallback(() => {
    if (creatorInternal && creatorInternal.current) {
      creatorInternal.current.reset();
    }
  }, []);

  return {
    changeBackground,
    toggleFoil,
    reset,
  };
};

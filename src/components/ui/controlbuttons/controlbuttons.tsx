import React, { useCallback, useEffect } from 'react';
import { Wrapper } from './controlbuttons.style';
import { Button } from '../button/button';
import { POPUP_MENU_TYPE, PopupMenu } from '../popupmenu/popupmenu';
import { POPUP_TYPE, usePopup } from './hooks/use-popup';
import { usePopupItemSelection } from '../popupmenu/hooks/use-popup-item-selection';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';
import { Creator } from '../../pixi/creator/creator';
import { backgrounds } from '../../../assets/data/data.json';

export function ControlButtons({
  onResetClick,
  onBackgroundClick,
  onFoilClick,
  onSendClick,
  creator,
}: {
  onResetClick: () => void;
  onBackgroundClick: (bgIndex: number) => void;
  onFoilClick: (foilIndex: number) => void;
  onSendClick: () => void;
  creator: Creator | null;
}) {
  const { activePopup, togglePopup, hide } = usePopup();
  const {
    app: { translations, language },
  } = useAppContext();

  useEffect(() => {
    if (creator) {
      creator.addCanvasClickListener(hide);
    }
  }, [hide, creator]);

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-creator',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-creator'],
    },
  ]);

  const {
    selectedIndexes: selectedBgIndexes,
    selectItem: selectBg,
    reset: resetBackgroundPopup,
  } = usePopupItemSelection({
    type: POPUP_MENU_TYPE.SINGLE,
    initSelectedIndexes: [0],
    onClick: onBackgroundClick,
  });

  const {
    selectedIndexes: selectedFoilIndexes,
    selectItem: selectFoil,
    reset: resetFoilPopup,
  } = usePopupItemSelection({
    type: POPUP_MENU_TYPE.MULTI,
    onClick: onFoilClick,
  });

  const resetAll = useCallback(() => {
    onResetClick();
    resetFoilPopup();
    resetBackgroundPopup();
    onBackgroundClick(0);
    hide();
  }, [
    onBackgroundClick,
    hide,
    onResetClick,
    resetFoilPopup,
    resetBackgroundPopup,
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['collage-screen-creator'];

  return (
    <Wrapper>
      <Button
        title={t['reset'][language]}
        onClick={resetAll}
        colorInactive={'#143b92'}
        colorActive={'#143b92'}
      />
      <Button
        title={t['foils'][language]}
        active={activePopup === POPUP_TYPE.FOIL}
        onClick={togglePopup(POPUP_TYPE.FOIL)}
        colorInactive={'#0058bc'}
        colorActive={'#ff004e'}>
        {activePopup === POPUP_TYPE.FOIL && (
          <PopupMenu
            selectedIndexes={selectedFoilIndexes}
            onClick={selectFoil}
          />
        )}
      </Button>
      <Button
        title={t['backgrounds'][language]}
        fontSize={language === 'en' ? 30 : 40}
        onClick={togglePopup(POPUP_TYPE.BACKGROUND)}
        active={activePopup === POPUP_TYPE.BACKGROUND}
        colorInactive={'#4d89da'}
        colorActive={'#ff004e'}>
        {activePopup === POPUP_TYPE.BACKGROUND && (
          <PopupMenu
            selectedIndexes={selectedBgIndexes}
            onClick={selectBg}
            thumbs={backgrounds}
          />
        )}
      </Button>
      <Button
        fontSize={language === 'ru' ? 30 : 40}
        title={t['send'][language]}
        onClick={onSendClick}
        colorInactive={'#3db6f5'}
        colorActive={'#ff004e'}
      />
    </Wrapper>
  );
}

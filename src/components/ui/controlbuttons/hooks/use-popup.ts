import { useCallback, useState } from 'react';

export enum POPUP_TYPE {
  FOIL = 'foil',
  BACKGROUND = 'background',
}

export const usePopup = () => {
  const [activePopup, setActivePopup] = useState<null | POPUP_TYPE>(null);

  const togglePopup = useCallback(
    (popupName: POPUP_TYPE) => () => {
      if (activePopup === popupName) {
        setActivePopup(null);
      } else {
        setActivePopup(popupName);
      }
    },
    [activePopup],
  );

  const hide = useCallback(() => {
    setActivePopup(null);
  }, []);

  return {
    hide,
    togglePopup,
    activePopup,
  };
};

import styled from 'styled-components';

export const Wrapper = styled.div<{
  top?: number;
  colorInactive: string;
  colorActive: string;
  active?: boolean;
}>`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  width: 235px;
  height: 110px;
  position: ${props => (props.top ? 'absolute' : 'relative')};
  top: ${props => props.top}px;
  background-color: ${props =>
    props.active ? props.colorActive : props.colorInactive};
  cursor: pointer;
`;

export const Title = styled.div<{ fontSize?: number }>`
  font-size: ${props => props.fontSize + 'px' || '40px'};
  color: #ffffff;
`;

export const Subtitle = styled.div<{ language?: string }>`
  font-size: 30px;
  color: #ffffff;
  font-family: ${props =>
    props.language === 'ru' ? 'NocturneMedium' : 'ManofaLight'};
`;

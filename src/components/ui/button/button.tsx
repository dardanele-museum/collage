import React from 'react';
import { Wrapper, Title, Subtitle } from './button.style';

export function Button({
  title,
  subtitle,
  onClick,
  top = 0,
  language = 'pl',
  active = true,
  colorActive = '#3db6f5',
  colorInactive = '#3db6f5',
  fontSize = 40,
  children,
}: {
  title: string;
  subtitle?: string;
  onClick?: () => void;
  top?: number;
  language?: string;
  active?: boolean;
  colorActive?: string;
  colorInactive?: string;
  children?: any;
  fontSize?: number;
}) {
  return (
    <Wrapper
      active={active}
      colorActive={colorActive}
      colorInactive={colorInactive}
      onClick={onClick}
      top={top}>
      <Title fontSize={fontSize}>{title}</Title>
      {subtitle && <Subtitle language={language}>{subtitle}</Subtitle>}
      {children}
    </Wrapper>
  );
}

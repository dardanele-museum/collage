import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  cursor: pointer;
  height: 400px;
  pointer-events: all;
`;

export const Image = styled.div`
  display: flex;
  justify-content: center;
  width: 235px;
  height: 255px;
`;



import React from 'react';
import { Wrapper, Image } from './thumbnail.style';
import { Button } from '../button/button';
import { ARTISTS, useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

const ARTISTS_DATA = {
  [ARTISTS.KANTOR]: {
    thumbnailSrc: 'assets/images/thumbs/kantor.jpg',
    title: '__TRANSLATED__',
    subtitle: '__TRANSLATED__',
  },
  [ARTISTS.SZAJNA]: {
    thumbnailSrc: 'assets/images/thumbs/szajna.jpg',
    title: '__TRANSLATED__',
    subtitle: '__TRANSLATED__',
  },
};

export function Thumbnail({
  onClick,
}: {
  onClick: () => void;
}) {
  const {
    app: { translations, language },
    game: { artist },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'collage-screen-artists',
      base: 'screens',
      path: ['collage-screens', 'collage-screen-artists'],
    },
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['collage-screen-artists'];

  ARTISTS_DATA[ARTISTS.SZAJNA].title = t['szajna'][language];
  ARTISTS_DATA[ARTISTS.SZAJNA].subtitle = t['collage'][language];
  ARTISTS_DATA[ARTISTS.KANTOR].title = t['kantor'][language];
  ARTISTS_DATA[ARTISTS.KANTOR].subtitle = t['assemblage'][language];

  return (
    <Wrapper onClick={onClick}>
      <Image>
        <img src={artist ? ARTISTS_DATA[artist].thumbnailSrc : ''} alt={'artist'}/>
      </Image>
      <Button
        title={artist ? ARTISTS_DATA[artist].title : ''}
        subtitle={artist ? ARTISTS_DATA[artist].subtitle : ''}
      />
    </Wrapper>
  );
}

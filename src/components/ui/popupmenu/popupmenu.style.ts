import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 235px;
  height: 65px;
  position: absolute;
  top: -85px;
`;

export const Item = styled.div<{ backgroundColor: string; selected: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${props => (props.selected ? 75 : 65)}px;
  height: ${props => (props.selected ? 75 : 65)}px;
  opacity: ${props => (props.selected ? 1 : 0.6)};
  background-color: ${props => props.backgroundColor || '#ffffff'};
`;

export const ItemThumb = styled.div<{ src?: string }>`
  width: 60px;
  height: 60px;
  background: url(${props => props.src}) no-repeat center;
  background-size: 60px;
`;

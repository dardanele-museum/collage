import { useCallback, useState } from 'react';
import { POPUP_MENU_TYPE } from '../popupmenu';

export const usePopupItemSelection = ({
  type = POPUP_MENU_TYPE.SINGLE,
  initSelectedIndexes = [],
  onClick,
}: {
  type: POPUP_MENU_TYPE;
  initSelectedIndexes?: number[];
  onClick: (index: number) => void;
}) => {
  const [selectedIndexes, setSelectedIndexes] = useState(initSelectedIndexes);

  const selectItem = useCallback(
    (index: number) => {
      onClick(index);
      if (type === POPUP_MENU_TYPE.SINGLE) {
        setSelectedIndexes([index]);
      } else {
        const arrIndex = selectedIndexes.indexOf(index);
        if (arrIndex >= 0) {
          const newIndexes = [...selectedIndexes];
          newIndexes.splice(arrIndex, 1);
          setSelectedIndexes(newIndexes);
        } else {
          setSelectedIndexes([...selectedIndexes, index]);
        }
      }
    },
    [onClick,selectedIndexes, type],
  );

  const reset = useCallback(() => {
    setSelectedIndexes(initSelectedIndexes);
  }, [initSelectedIndexes]);

  return {
    reset,
    selectedIndexes,
    selectItem,
  };
};

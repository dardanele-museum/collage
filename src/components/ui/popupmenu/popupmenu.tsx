import React, { useCallback } from 'react';
import { Wrapper, Item, ItemThumb } from './popupmenu.style';

export enum POPUP_MENU_TYPE {
  SINGLE = 'single',
  MULTI = 'multi',
}

const ITEMS = [
  {
    key: 0,
    backgroundColor: '#003365',
  },
  {
    key: 1,
    backgroundColor: '#9f0000',
  },
  {
    key: 2,
    backgroundColor: '#005500',
  },
];

export function PopupMenu({
  onClick,
  selectedIndexes,
  thumbs,
}: {
  onClick: (index: number) => void;
  selectedIndexes: number[];
  thumbs?: string[];
}) {
  const onClickInternal = useCallback(
    index => (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      onClick(index);
      event.stopPropagation();
    },
    [onClick],
  );

  return (
    <Wrapper>
      {ITEMS.map((item, index) => (
        <Item
          key={item.key}
          selected={selectedIndexes.indexOf(index) !== -1}
          backgroundColor={item.backgroundColor}
          onClick={onClickInternal(index)}>
          {thumbs && <ItemThumb src={thumbs?.[index]} />}
        </Item>
      ))}
    </Wrapper>
  );
}

import React, { memo } from 'react';
import { InitAppParameters, useAppData } from './hooks/use-app-data';
import { AppContext } from './hooks/use-app-context';

export const AppData = memo(function AppData({
  initParameters,
  children,
}: {
  initParameters: InitAppParameters;
  children: any;
}) {
  const contextValue = useAppData(initParameters);

  return (
    <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
  );
});

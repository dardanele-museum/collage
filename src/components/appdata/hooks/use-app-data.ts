import { useCallback, useEffect, useMemo, useState } from 'react';
import { useApiCall } from '../../../hooks/use-api-calls';
import { ARTISTS } from './use-app-context';

export type InitAppParameters = {
  language: string;
  tag: string;
  groupCount: number;
  path: string;
  resourcesPath: string;
};

export function useAppData(initParameters: InitAppParameters) {
  const { language, tag, groupCount, path, resourcesPath } = useMemo(
    () => initParameters,
    [initParameters],
  );

  const [translations, setTranslations] = useState(null);
  const [artist, setArtist] = useState<ARTISTS | null>(null);
  const { getTranslations } = useApiCall({ resourcesPath });

  const chooseArtist = useCallback((artist: ARTISTS) => {
    setArtist(artist);
  }, []);

  useEffect(() => {
    getTranslations().then(data => setTranslations(data));
  }, [getTranslations, language]);

  return {
    game: {
      artist,
      chooseArtist,
    },
    app: {
      language,
      tag,
      groupCount,
      path,
      translations,
    },
  };
}

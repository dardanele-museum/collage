import React, { useContext } from 'react';

export enum ARTISTS {
  SZAJNA = 'szajna',
  KANTOR = 'kantor',
}

export const AppContext = React.createContext<{
  game: {
    artist: ARTISTS | null;
    chooseArtist: (artist: ARTISTS) => void;
  };
  app: {
    language: string;
    tag: string;
    groupCount: number;
    path: string;
    translations: any;
  };
}>({
  game: {
    artist: null,
    chooseArtist: (artist: ARTISTS) => {},
  },
  app: {
    language: 'pl',
    tag: '',
    groupCount: 1,
    path: 'interactive',
    translations: null,
  },
});

export function useAppContext() {
  const { game, app } = useContext(AppContext);
  return {
    game,
    app,
  };
}

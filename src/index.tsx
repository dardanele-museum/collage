import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Application } from './components/application';
import reportWebVitals from './reportWebVitals';
import { app as appConfig } from './config.json';
import { AppData } from './components/appdata/app-data';
import { ApplicationWrapper } from './index.style';

const initParameters = {
  language: 'ru',
  groupCount: 5,
  path: 'interactive',
  tag: '0',
  resourcesPath: '',
};

if (window.require) {
  const remote = window.require('electron').remote;
  const argv = remote.getGlobal('argv');

  if (argv.lang) {
    initParameters.language = argv.lang.substring(0,2); // e.g. pl-PL to pl
  }
  if (argv.groupCount) {
    initParameters.groupCount = argv.groupCount;
  }
  if (argv.path) {
    initParameters.path = argv.path;
  }
  if (argv.tag) {
    initParameters.tag = argv.tag;
  }

  initParameters.resourcesPath = remote.getGlobal('resourcesPath');
}

const root = document.getElementById('root');
if (root) {
  root.style.transformOrigin = 'top left';
}

ReactDOM.render(
  <React.StrictMode>
    <AppData initParameters={initParameters}>
      <ApplicationWrapper language={initParameters.language}>
        <Application />
      </ApplicationWrapper>
    </AppData>
  </React.StrictMode>,
  root,
);

const onResize = (e: any) => {
  if (root) {
    const { innerWidth, innerHeight } = e.target;
    const scaleX = innerWidth > 0 ? innerWidth / appConfig.width : 1;
    const scaleY = innerHeight > 0 ? innerHeight / appConfig.height : 1;
    const scale = scaleX > scaleY ? scaleY : scaleX;
    root.style.transform = `scale(${scale})`;
  }
};

window.onresize = onResize;
window.onload = () => {
  const e = {
    target: {
      innerHeight: window.innerHeight,
      innerWidth: window.innerWidth,
    },
  };
  onResize(e);
};

if (root) {
  const rect = root.getBoundingClientRect();
  onResize({
    target: {
      innerWidth: rect.width,
      innerHeight: rect.height,
    },
  });
}
// If you want to screensaver measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

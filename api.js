const axios = require('axios');

const apiData = {
  token: '',
};

const LOGIN_ENDPOINT = '/apiaccount/login';
const UPLOAD_USER_IMAGE_ENDPOINT = '/portal/UploadUserImages';
const USER_EXISTS_IN_DB = '/portal/UserExistInDb';

const login = async ( { host, login, password } ) => {
  try {
    const result = await axios.post(`${host}${LOGIN_ENDPOINT}`, {
      username: login,
      password
    }, {
      'Content-Type': 'application/json',
    });
    apiData.token = result.data.token;
  } catch (error) {
    console.log('Error during login process, ', error);
  }
};

const userExistsInDB = async ({ host, email }) => {
  const url = `${host}${USER_EXISTS_IN_DB}`;
  const data = {
    email,
  };

  try {
    const result = await axios.post(url, data, {
      headers: {
        Authorization: `Bearer ${apiData.token}`,
        'Content-Type': 'application/json',
      },
    });
    return result.data;
  } catch (error) {
    console.log('Error while checking user: ', error);
  }
};

const uploadUserImage = async ({ host, userKey, appName, mediaPaths }) => {
  const url = `${host}${UPLOAD_USER_IMAGE_ENDPOINT}`;
  const data = {
    appName,
    userKey,
    mediaPaths,
  };

  try {
    const result = await axios.post(url, data, {
      headers: {
        Authorization: `Bearer ${apiData.token}`,
        'Content-Type': 'application/json',
      },
    });
    return result.status;
  } catch (error) {
    console.log('Error while checking email: ', error);
  }
};

module.exports = {
  login,
  uploadUserImage,
  userExistsInDB
};
